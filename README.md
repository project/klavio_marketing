# Klavio marketing

Provides a webform handler to send data to Klavio and three dashboard pages displaying Klavio information.

## Usage

Configure the Klavio public and private key at `/admin/config/services/klaviyo`

Activate the Klavio Marketing widget and formatter.

The dashboard pages are located at these urls respectively

 * Dashboards dashboard page: `/admin/klaviyo/campaigns`
 * Contacts dashboard page: `/admin/klaviyo/campaigns`
 * Lists dashboard page: `/admin/klaviyo/lists`

To use the webformhandler, you need to specify the fields from which data should be forwarded to Klavio, the default fields can be chosen via select list.

When using the custom yaml mapping the following syntax is required:
 * `field[klavio_field_id,0]: '[webform_field_machine_name]'`.

Examples using Klavio custom mapping:
 * Field ID: `field[345,0]: '[webform_field_machine_name]'`
 * Personalization Tag: `field[%PERS_1%,0]: '[webform_field_machine_name]'`

More info can be found at the Klavio API pages, [contact sync documentation](https://developers.klaviyo.com/en/reference/create_client_subscription) and info on where to find the lists can be found [here](https://developers.klaviyo.com/en/reference/get_lists).
