<?php

namespace Drupal\klavio_marketing;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use KlaviyoAPI\KlaviyoAPI;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Api service.
 */
class KlavioMarketingApi
{
    /**
     * The klavio marketing api SDK.
     *
     * @var \KlavioMarketing
     */
    protected $klavioMarketingSDK;

    /**
     * The config factory.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;
    protected $company_id;
    protected $private_key;


    /**
     * Constructs an Api object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The configuration factory.
     */
    public function __construct(ConfigFactoryInterface $config_factory)
    {
        $this->configFactory = $config_factory;
        $config = $this->configFactory->get('klavio_marketing.settings');
        // $this->klavioMarketingSDK = new \KlavioMarketing($config->get('company_id'), $config->get('private_key'));

        $this->company_id = $config->get('company_id');
        $this->private_key = $config->get('private_key');
        $this->klavioMarketingSDK = new KlaviyoAPI(
            $config->get('private_key'),
            $num_retries = 3,
            $wait_seconds = 3,
            $guzzle_options = []
        );
    }

    /**
     * Get lists via the klavio marketing API.
     *
     * @param array $filter_options
     *   The filter options for the api call.
     * @param int $page
     *   The page to start from.
     * @param int $limit
     *   The number of records per page.
     */
    public function getLists(array $filter_options = [], int $page = 0, int $limit = 20)
    {
        $url = 'https://a.klaviyo.com/api/lists/';
        $headers = [
        'Authorization' => 'Klaviyo-API-Key '.$this->private_key,
        'accept' => 'application/json',
        'revision' => '2023-02-22',
        ];

        $response_data = array();

        try {

            $client = new Client();
            $response = $client->request('GET', $url, [
                'headers' => $headers,
            ]);

            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();

            $decoded_response =  json_decode($responseBody);


            if(isset($decoded_response->data)) {
                $response_data = $decoded_response->data;
            }

        } catch (\Exception $e) {
            log_klavio_marketing_messages("warning", $e->getMessage());
        }

        return $response_data;
    }

    /**
     * Get Campaigns via the klavio marketing API.
     *
     * @param array $filter_options
     *   The filter options for the api call.
     * @param int $page
     *   The page to start from.
     * @param int $limit
     *   The number of records per page.
     */
    public function getCampaigns(array $filter_options = [], int $page = 0, int $limit = 20)
    {

        $url = 'https://a.klaviyo.com/api/campaigns/';
        $headers = [
         'Authorization' => 'Klaviyo-API-Key '.$this->private_key,
         'accept' => 'application/json',
         'revision' => '2023-02-22',
     ];

        $response_data = array();

        try {

            $client = new Client();
            $response = $client->request('GET', $url, [
                'headers' => $headers,
            ]);

            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();

            $decoded_response =  json_decode($responseBody);

            if(isset($decoded_response->data)) {
                $response_data = $decoded_response->data;
            }

        } catch (\Exception $e) {

        }

        return $response_data;

    }

    /**
     * Sends contact data to klavio marketing using the klavio marketing API.
     *
     * @param array $contact
     *   The data to send to the klavio marketing site.
     */
    public function syncContact(array $contact)
    {
        $company_id = $this->company_id;
        $email = $contact['email'];
        $listId = $contact['list_id'];

        $url = 'https://a.klaviyo.com/client/subscriptions/?company_id='.$company_id;
        $headers = [
            'content-type' => 'application/json',
            'revision' => '2023-02-22',
        ];

        $data = [
            "data" => [
                "type" => "subscription",
                "attributes" => [
                    "list_id" => $listId,
                    "custom_source" => $contact['source']??'',
                    "email" => $contact['email'],
                    "phone_number" => $contact['phone_number']??'',
                    "properties" => [
                        "newKey" => "New Value"
                    ]
                ]
            ]
        ];

        try {

            $client = new Client();
            $response = $client->request('POST', $url, [
                'headers' => $headers,
                'json' => $data,
            ]);

            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();

        } catch (\Exception $e) {
            log_klavio_marketing_messages("warning", $e->getMessage());
        }
    }

}
