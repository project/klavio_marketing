<?php

namespace Drupal\klavio_marketing\Form;

/**
 * Adds a lists' dashboard.
 */
class KlavioMarketingLists extends KlavioMarketingDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 900): array {
    $data['table_fields'] = [
      $this->t('Id'),
      $this->t('Name'),
    ];

    $response = $this->api->getLists([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = count($response);
      foreach ($response as $key => $list) {
        $data['rows'][$key] = [
          $list->id,
          $list->attributes->name,
        ];
      }
    }
    return $data;
  }
}
