<?php

namespace Drupal\klavio_marketing\Form;

use Drupal\Core\Link;

/**
 * Adds a campaigns dashboard.
 */
class KlavioMarketingCampaigns extends KlavioMarketingDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['table_fields'] = [
      $this->t('ID'),
      $this->t('Name'),
      $this->t('Status'),
    ];

    // Check if we have an object containing our data instead of an error
    // message.
    $response = $this->api->getCampaigns([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = count($response);
      foreach ($response as $key => $campaign) {
        // Check if the row is actual data or metadata from the api.
        $data['rows'][$key] = [
          $campaign->id,
          $campaign->attributes->name,
          $campaign->attributes->status,
        ];

      }
    }

    return $data;
  }

  /**
   * Calculates the percentage rate of a field.
   *
   * @param int $amount
   *   The amount to use in the calculation.
   * @param int $total
   *   The total to use in the calculation.
   *
   * @return string
   *   Returns the field rate or defaults to "-", if the total given is zero.
   */
  private function convertToRate(int $amount, int $total): string {
    if ($total > 0) {
      $rate = ($amount / $total) * 100;
      return round($rate, 2) . '%';
    }
    else {
      return '-';
    }
  }

}
