<?php

namespace Drupal\klavio_marketing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Klavio marketing settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Api connection'),
    ];
    $form['api']['company_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company Public API Key'),
      '#default_value' => $this->config('klavio_marketing.settings')->get('company_id'),
      '#description' => $this->t('Your Public API Key / Site ID. See this <a href="https://help.klaviyo.com/hc/en-us/articles/115005062267" target="_blank">article</a> for more details.'),
      '#required' => TRUE
    ];

    $form['api']['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#default_value' => $this->config('klavio_marketing.settings')->get('private_key'),
      '#description' => $this->t('Your Private Key. See this <a href="https://help.klaviyo.com/hc/en-us/articles/7423954176283" target="_blank">article</a> for more details.'),
      '#required' => TRUE
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['klavio_marketing.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'klavio_marketing_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('klavio_marketing.settings')
    ->set('company_id', $form_state->getValue('company_id'))
    ->set('private_key', $form_state->getValue('private_key'))
    ->save();
    parent::submitForm($form, $form_state);
  }

}
