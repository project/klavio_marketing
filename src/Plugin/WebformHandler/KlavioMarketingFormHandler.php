<?php

namespace Drupal\klavio_marketing\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\webformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "klavio_marketing_contact",
 *   label = @Translation("Klavio Marketing"),
 *   category = @Translation("Automated marketing"),
 *   description = @Translation("Send submission data to Klavio Marketing"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class KlavioMarketingFormHandler extends WebformHandlerBase {

  /**
   * The klavio marketing api service.
   *
   * @var \Drupal\klavio_marketing\KlavioMarketingApi
   */
  protected $klavioMarketingApi;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->klavioMarketingApi = $container->get('klavio_marketing.api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'list_id' => NULL,
      'email_field' => NULL,
      'first_name_field' => NULL,
      'last_name_field' => NULL,
      'phone_field' => NULL,
      'debug' => FALSE,
      'custom_klavio_marketing_field' => "field[klavio_marketing_field_id,0]: '[webform_field_machine_name]'",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $elements = $this->webform->getElementsInitializedAndFlattened();

    $provider = \Drupal::service('klavio_marketing.api');

    $response = $provider->getLists([], $page=0, $limit=100);

    $list_ids = ['' => $this->t('None')];

    if(is_array($response) && !empty($response)) {
      foreach ($response as $key => $list) {
        $list_ids[$list->id] = $list->attributes->name;
      }
    }

    // Add the select none option to the list.
    $options = ['' => $this->t('None')];

    // Add every field to the list as a select option.
    foreach ($elements as $key => $element) {
      // Check if the field is an element field and not a layout field.
      if (isset($element['#title'])) {
        $options[$key] = $element['#title'];
      }
    }

    $form['email_field'] = [
      '#title' => $this->t('Email field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the email field to sync.'),
      '#default_value' => $this->configuration['email_field'],
      '#required' => TRUE,
    ];
    $form['list_id'] = [
      '#title' => $this->t('List id'),
      "#type" => "select",
      "#options" => $list_ids,
      '#description' => $this->t('Choose the list id to sync.'),
      '#default_value' => $this->configuration['list_id'],
      '#required' => TRUE,
    ];
    $form['first_name_field'] = [
      '#title' => $this->t('First name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the first name field to sync.'),
      '#default_value' => $this->configuration['first_name_field'],
    ];
    $form['last_name_field'] = [
      '#title' => $this->t('Last name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the last name field to sync.'),
      '#default_value' => $this->configuration['last_name_field'],
    ];
    $form['phone_field'] = [
      '#title' => $this->t('Phone number'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the phone number field to sync.'),
      '#default_value' => $this->configuration['phone_field'],
    ];

    // Enable the debug option.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];
    $form['development']['custom_klavio_marketing_field'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Klavio marketing submission data mapping'),
      '#description' => $this->t("Edit the form data that will be sent to the KlavioMarketing site when a webform submission is made.
        The following syntax is required:
        <code>field[klavio_marketing_field_id,0]: '[webform_field_machine_name]'<code>.
        Examples <code>field[345,0]: '[field_machine_name]'<code> or <code>field[%PERS_1%,0]: '[field_machine_name]'<code>.
        More info can be found at the klavio marketing api 1, contact sync documentation."),
      '#default_value' => $this->configuration['custom_klavio_marketing_field'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    parent::submitConfigurationForm($form, $form_state);

    // Save the configuration settings.
    $this->applyFormStateToConfiguration($form_state);

    // Save debug settings.
    $this->configuration['debug'] = (bool) $this->configuration['debug'];

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    $data = $webform_submission->getData();

    // Retrieving the configured klavio marketing fields from the data.
    $email_key = $settings['email_field'];
    $list_id_key = $settings['list_id'];
    $first_name_key = $settings['first_name_field'];
    $last_name_key = $settings['last_name_field'];
    $phone_key = $settings['phone_field'];
    $custom_klavio_marketing_field = $settings['custom_klavio_marketing_field'];

    // $form_id = $this->getFormId();

    $contact_properties['source'] = $this->webform->label();

    // Email field doesn't need to be checked, because it's required.
    $contact_properties['email'] = $data[$email_key];

    // Email field doesn't need to be checked, because it's required.
    $contact_properties['list_id'] = $data[$list_id_key]??$list_id_key;

    // Check if first name field has a value.
    if (isset($data[$first_name_key]) && !empty($data[$first_name_key])) {
      $contact_properties['first_name'] = $data[$first_name_key];
    }

    // Check if last name field has a value.
    if (isset($data[$last_name_key]) && !empty($data[$last_name_key])) {
      $contact_properties['last_name'] = $data[$last_name_key];
    }
    // Check if last name field has a value.
    if (isset($data[$phone_key]) && !empty($data[$phone_key])) {
      $contact_properties['phone_number'] = $data[$phone_key];
    }

    $custom_klavio_marketing_fields = Yaml::parse($custom_klavio_marketing_field);

    if (!empty($custom_klavio_marketing_fields)) {
      // Check if the default value was changed.
      if (!in_array('field[klavio_marketing_field_id,0]', $custom_klavio_marketing_fields)) {

        foreach ($custom_klavio_marketing_fields as $key => $value) {
          // Remove the unneeded characters from the webform id.
          $webform_id = str_replace('[', '', $value);
          $webform_id = str_replace(']', '', $webform_id);

          // Check if the mapped field has data to send.
          if (!empty($data[$webform_id])) {
            $contact_properties[$key] = $data[$webform_id];
          }
        }
      }
    }

    $response = $this->klavioMarketingApi->syncContact($contact_properties);

    if ($settings['debug'] === TRUE) {
      $build = [
        'form_values_label' => ['#markup' => $this->t('Submitted form values are:')],
        'form_values_data' => [
          '#markup' => WebformYaml::encode($data),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
        'campaign_values_label' => ['#markup' => $this->t('Submitted klavio marketing values are:')],
        'campaign_values_data' => [
          '#markup' => WebformYaml::encode($contact_properties),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
      ];

      $message = $this->renderer->renderPlain($build);
      $this->getLogger('klavio_marketing')->debug(
        'Debugging enabled: </br>' . $message
      );
    }

  }

}
